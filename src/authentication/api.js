'use-strict';

const express = require('express');
const bodyParser = require('body-parser');
const config = require('../../config');
const model = require('../profile/model');
const logging = require('../../lib/logging');
const responseutil = require('../util/responseutil');
const tokenutil = require('../util/tokenutil');

const router = express.Router();
router.use(bodyParser.json());

router.post('/', (req, res, next) => {
    const body = req.body;
    model.getByEmailAndPassword(body.email, body.password, (err, entity) => {
        if (err) {
          next(responseutil.internal_error(err));
          return;
        }

        logging.info(entity);

        if (!entity || !entity.enable) {
          next(responseutil.bad_request());
          return;
        }

        let user = {
          id: entity.id,
          username: entity.email,
          firstname: entity.firstname,
          lastname: entity.lastname
        }
        res.json(responseutil.ok(tokenutil.createToken(user)));
    });
});


module.exports = router;
