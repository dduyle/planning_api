'use-strict';

const Datastore = require('@google-cloud/datastore');
const config = require('../../config');
const logging = require('../../lib/logging');
const validator = require('validator');
const stringutil = require('../util/stringutil');
const datastoreutil = require('../util/datastoreutil');

const ds = datastoreutil.ds();

const kind = 'Planning';

function jsonToDto(json) {
    let planning = {};
    planning.name = json.name;
    planning.descript = json.description;
    planning.address = json.address;
    planning.owner = json.owner;
    planning.admins = json.admins;
    planning.registers = json.registers;
    planning.createAt = json.createAt;
    planning.registries = json.registries;

    return planning;
}

function getPlanningsAsOwner(user_id, cb) {
    let query = ds.createQuery([kind])
        .filter('owner', '=', user_id);
    ds.runQuery(query, (error, entities) => {
        console.info(entities);
        if (error) {
            cb(error, null);
            return;
        }
        if (!entities) {
            return (null, null);
            return;
        }

        cb(null, datastoreutil.fromDatastores(entities));
        return;
    });
}

function getPlanningsAdmin(user_id, cb) {
    console.info(user_id);
    let query = ds.createQuery([kind])
        .filter('admins', '=', user_id);
    ds.runQuery(query, (error, entities) => {
        if (error) {
            cb(error, null);
            return;
        }
        if (!entities) {
            return (null, null);
            return;
        }

        cb(null, datastoreutil.fromDatastores(entities));
        return;
    });
}

function get(id, cb) {
    const key = ds.key([kind, parseInt(id, 10)]);
    ds.get(key, (err, entity) => {
        if (err) {
            cb(err);
            return;
        }
        if (!entity) {
            cb({
                code: 404,
                message: 'Not found'
            });
            return;
        }
        cb(null, datastoreutil.fromDatastore(entity));
        return;
    });
}

function update(id, dataBody, cb) {
    let data = jsonToDto(dataBody);

    let key;
    if (id) { // update
        key = ds.key([kind, parseInt(id, 10)]);
    } else {
        key = ds.key(kind);

    }

    const entity = {
        key: key,
        data: datastoreutil.convertToDatastore(data, ['description'])
    };

    ds.save(
        entity,
        (err) => {
            if (err) {
                cb(err);
                return;
            }
            data.id = entity.key.id;
            cb(null, data);
            return;
        }
    );
}

module.exports = {
    create: (data, cb) => {
        update(null, data, cb);
    },
    update: update,
    get: get,
    getPlanningsAsOwner: getPlanningsAsOwner,
    getPlanningsAdmin: getPlanningsAdmin
};
