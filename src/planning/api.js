'use-strict';

const express = require('express');
const bodyParser = require('body-parser');
const config = require('../../config');
const model = require('./model');
const profile = require('../profile/model');
const responseutil = require('../util/responseutil');
const constants = require('../util/constants');
const validator = require('validator');

let router = express.Router();
router.use(bodyParser.json());

/**
 * reference_begin: beginning's timestamp
 * reference_end: ending's timestamp
 * tocompare: timestamp needs to compare
 *
 * true if interseted, false if not
 */
function timeIntersection(reference_begin, reference_end, tocompare) {
    return reference_begin <= tocompare && reference_end >= tocompare;
}

function verifyInsertPlanning(data) {

    if (validator.isEmpty(data.reason) ||
        validator.isEmpty(data.planning_id) ||
        !validator.isNumeric(data.schedule_begin) ||
        !validator.isNumeric(data.schedule_end)) {
        return false;
    }

    if (data.schedule_end <= data.schedule_begin) {
        return false;
    }

    let today = new Date().getTime();
    let diff_begin = data.schedule_begin - today;

    if (diff_begin < 1 * 60 * 1000) {
        return false;
    }

    return true;
}

/**
 * POST /api/planning
 *
 * Create a new planning
 */
router.post('/', (req, res, next) => {

    profile.getByEmail(req.token.username, (err, user) => {
        if (err) {
            next(responseutil.internal_error(err));
            return;
        }

        if (!user.enable) {
            next(responseutil.unauthorized());
            return;
        }

        let data = req.body;

        data.registries = [];
        data.owner = user.id;
        data.admins = [];
        data.registers = [];
        data.createAt = new Date();

        model.create(req.body, (err, entity) => {
            if (err) {
                responseutil.internal_error(err);
                return;
            }

            res.json(responseutil.created(entity));
        });
    });

});


/*
 *
 *
 *
 * User cancels his reservation
 */
router.put('/cancel', (req, res, next) => {
    profile.getByEmail(req.token.username, (err, user) => {
        if (err) {
            next(responseutil.internal_error(err));
            return;
        }

        if (!user.enable) {
            next(responseutil.unauthorized());
            return;
        }

        const planning_id = req.body.planning_id;
        const registry_id = req.body.registry_id;
        const user_id = user.id;

        model.get(req.body.planning_id, (err, planning) => {
            if (err || planning == null) {
                next(responseutil.conflict('planning not exist'));
                return;
            }

            let index = -1;
            let registry = null;
            for (let i = 0; len = planning.registries.length, i++; i < len) {
                if (i == registry_id) {
                    index = i;
                    registry = planning.registries[i];
                    break;
                }
            }

            if (index == -1) {
                next(responseutil.internal_error('Schudule not exist'));
                return;
            }

            if (user_id != registry.register) {
                next(responseutil.unauthorized());
                return;
            }

            // REMOVE this registry
            planning.registries.splice(index, 1);

            model.update(planning.id, planning, (err, data) => {
                if (err) {
                    next(responseutil.internal_error('error while updating planning'));
                    return;
                }

                res.json(responseutil.ok(data));
                return;
            });
        });
    });
});

/**
 **
 **
 ** Reject a reservation by admin or owner
 */
router.put('/reject', (req, res, next) => {
    profile.getByEmail(req.token.username, (err, user) => {
        if (err) {
            next(responseutil.internal_error(err));
            return;
        }

        if (!user.enable) {
            next(responseutil.unauthorized());
            return;
        }

        const planning_id = req.body.planning_id;
        const registry_id = req.body.registry_id;
        const user_id = user.id;

        model.get(req.body.planning_id, (err, planning) => {
            if (err || planning == null) {
                next(responseutil.conflict('planning not exist'));
                return;
            }

            if (planning.owner != user_id && planning.admins.indexOf(user_id) == -1) {
                next(responseutil.unauthorized());
                return;
            }

            let index = -1;

            for (let i = 0; len = planning.registries.length, i < len; i++) {
                if (i == registry_id) {
                    index = i;
                    break;
                }
            }

            if (index == -1) {
                next(responseutil.internal_error('Schudule not exist'));
                return;
            }

            // REMOVE this registry
            planning.registries.splice(index, 1);

            model.update(planning.id, planning, (err, data) => {
                if (err) {
                    next(responseutil.internal_error('error while updating planning'));
                    return;
                }

                res.json(responseutil.ok(data));
                return;
            });
        });
    });
});

/**
 *
 *
 * validate a scheduled planning
 */
router.put('/validate', (req, res, next) => {
    profile.getByEmail(req.token.username, (err, user) => {
        if (err) {
            next(responseutil.internal_error(err));
            return;
        }

        if (!user.enable) {
            next(responseutil.unauthorized());
            return;
        }

        const planning_id = req.body.planning_id;
        const registry_id = req.body.registry_id;
        const user_id = user.id;

        model.get(req.body.planning_id, (err, planning) => {
            if (err || planning == null) {
                next(responseutil.conflict(err ? err : 'planning not exist'));
                return;
            }

            if (planning.owner != user_id && planning.admins.indexOf(user_id) == -1) {
                next(responseutil.unauthorized());
                return;
            }

            let registry = null;

            let registries = planning.registries;
            for (let i = 0; len = registries.length, i < len; i++) {
                if (registries[i].id == registry_id) {
                    registry = registries[i];
                    break;
                }
            }

            if (registry == null || registry == undefined) {
                next(responseutil.internal_error('Schudule not exist'));
                return;
            }

            if (registry.state != constants.PENDING) {
                next(responseutil.internal_error('Schedule validated'));
                return;
            }

            registry.state = constants.VALIDATED;

            model.update(planning.id, planning, (err, data) => {
                if (err) {
                    next(responseutil.internal_error('error while updating planning'));
                    return;
                }

                res.json(responseutil.ok(data));
                return;
            });
        });
    });

});

/**
 * PUT api/planning/register
 *
 * Json post
 { planning_id
   schedule_begin
   schedule_end
   reason
 }
 *
 * Register a planning
 */
router.put('/register', (req, res, next) => {
    if (!verifyInsertPlanning(req.body)) {
        next(responseutil.bad_request('Invalid data'));
        return;
    }

    profile.getByEmail(req.token.username, (error, user) => {
        if (error) {
            next(responseutil.internal_error(error));
            return;
        }

        if (!user.enable) {
            next(responseutil.unauthorized());
            return;
        }

        let data = req.body;
        let user_id = user.id;
        console.info("planning_id " + req.body.planning_id);
        model.get(req.body.planning_id, (err, planning) => {
            if (err || planning == null) {
                next(responseutil.conflict('planning not exist'));
                return;
            }

            const registries = planning.registries;
            let can_insert = true;
            const r_begin = data.schedule_begin;
            const r_end = data.schedule_end;

            for (let i = 0; len = registries.length, i < len; i++) {
                const register = registries[i];
                const r_schedule_begin = register.schedule_begin;
                const r_schedule_end = register.schedule_end;

                if (timeIntersection(r_begin, r_end, r_schedule_begin) &&
                    timeIntersection(r_begin, r_end, r_schedule_end)) {
                    can_insert = false;
                    break;
                }
            }

            if (!can_insert) {
                next(responseutil.conflict('Schudule occupied'));
                return;
            }

            if (!planning.registers.indexOf(user_id)) {
                planning.registers.push(user_id);
            }

            const newRegister = {
                register: user_id,
                schedule_begin: r_begin,
                schedule_end: r_end,
                reason: data.reason,
                state: constants.PENDING,
                id: planning.registries.length
            }

            planning.registries.push(newRegister);
            model.update(planning.id, planning, (err, data) => {
                if (err) {
                    next(responseutil.internal_error('error while updating planning'));
                    return;
                }

                res.json(responseutil.ok(data));
                return;
            });
        });
    });
});

/**
 * GET /api/planning
 *
 * Get all my plannings as owner or admin
 */
router.get('/admin', (req, res, next) => {
    model.getPlanningsAdmin(req.token.id, (err, planningsAdmin) => {
        if (err) {
            next(responseutil.internal_error('Error while getting plannings as admin for this user'));
            return;
        }

        res.json(responseutil.ok(planningsAdmin));
        return;
    });

});

/**
 * GET /api/owner
 *
 * Get all my plannings as owner or admin
 */
router.get('/owner', (req, res, next) => {
    model.getPlanningsAsOwner(req.token.id, (err, planningsOwner) => {
        if (err) {
            next(responseutil.internal_error('Error while getting plannings as admin for this user'));
            return;
        }

        res.json(responseutil.ok(planningsOwner));
        return;
    });

});

module.exports = router;
