'use-strict';

const PENDING = "PENDING";
const VALIDATED = "VALIDATED";

module.exports = {
    PENDING: PENDING,
    VALIDATED: VALIDATED
}
