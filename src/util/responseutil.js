'use-strict';

const OK = 200;
const CREATED = 201;
const ACCEPTED = 202;
const NO_CONTENT = 204;
const BAD_REQUEST = 400;
const UNAUTHORIZED = 401;
const NOT_FOUND = 404;
const CONFLICT = 409;
const INTERNAL_ERROR = 500;


function created(object) {
    return {
        status: CREATED,
        data: object
    };
}

function ok(object) {
    return {
        status: OK,
        data: object
    };
}

function internal_error(message) {
    return {
        status: INTERNAL_ERROR,
        error: message
    }
}

function no_content() {
    return {
        status: NO_CONTENT,
        data: null
    };
}

function bad_request(error) {
    return {
        status: BAD_REQUEST,
        error: error
    };
}

function conflict(error) {
    return {
        status: CONFLICT,
        error: error
    };
}

function unauthorized() {
    return {
        status: UNAUTHORIZED,
        message: 'Unauthorized'
    };
}

module.exports = {
    created: created,
    ok: ok,
    no_content: no_content,
    bad_request: bad_request,
    conflict: conflict,
    internal_error: internal_error,
    unauthorized: unauthorized
};
