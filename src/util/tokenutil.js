const config = require('../../config');
const jwt = require('jsonwebtoken');

function createToken(user) {
    const token = jwt.sign(user, config.get('TOKEN_SECRET'), {
        expiresIn: 86400 // expires in 24 hours
    });

    return {
        access_token: token,
        type: config.get('TOKEN_TYPE'),
        expires: 86400
    }
}

function verify(token) {
    try {
        let decoded = jwt.verify(token, config.get('TOKEN_SECRET'));
        return decoded;
    } catch (error) {
        return null;
    }
}

module.exports = {
    createToken: createToken,
    verify: verify
};
