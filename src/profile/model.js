'use-strict';

const Datastore = require('@google-cloud/datastore');
const config = require('../../config');
const logging = require('../../lib/logging');
const crypto = require('crypto');
const validator = require('validator');
const stringutil = require('../util/stringutil');

const ds = Datastore({
    projectId: config.get('GCLOUD_PROJECT')
});

const kind = 'Profile';

/**
 * generates random string of characters i.e salt
 * @function
 * @param {number} length - Length of the random string.
 */
function generateRandomString() {
    return new Buffer(config.get('SALT'), 'utf-8');
}

function sha512(password, salt) {
    let hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return value;
}

function saltHashPassword(password) {
    const salt = generateRandomString();
    const passwordHashed = sha512(password, salt);

    return passwordHashed;
}


/** Sync */
function randomString(length, chars) {
    if (!chars) {
        throw new Error('Argument \'chars\' is undefined');
    }

    const charsLength = chars.length;
    if (charsLength > 256) {
        throw new Error('Argument \'chars\' should not have more than 256 characters' +
            ', otherwise unpredictability will be broken');
    }

    const randomBytes = crypto.randomBytes(length);
    const result = sha512(chars, randomBytes);
    return result;
}

function isValidData(data) {
    return !validator.isEmpty(data.password) &&
        data.password.length >= 8 &&
        !validator.isEmpty(data.email) &&
        !validator.isEmpty(data.firstname) &&
        !validator.isEmpty(data.lastname) &&
        validator.isNumeric(data.telephone);
}

function dataFormatFromBody(body) {
    let data = {};
    data.password = body.password;
    data.email = body.email;
    data.firstname = body.firstname;
    data.lastname = body.lastname;
    data.telephone = body.telephone;
    data.phontable = typeof body.phontable != undefined ? body.phontable : false;
    data.enable = typeof body.enable != undefined ? body.enable : false;
    return data;
}

// Translates from Datastore's entity format to
// the format expected by the application.
//
// Datastore format:
//   {
//     key: [kind, id],
//     data: {
//       property: value
//     }
//   }
//
// Application format:
//   {
//     id: id,
//     property: value
//   }
function fromDatastore(obj) {
    obj.id = obj[Datastore.KEY].id;
    obj.key = obj[Datastore.KEY];
    return obj;
}

// Translates from the application's format to the datastore's
// extended entity property format. It also handles marking any
// specified properties as non-indexed. Does not translate the key.
//
// Application format:
//   {
//     id: id,
//     property: value,
//     unindexedProperty: value
//   }
//
// Datastore extended format:
//   [
//     {
//       name: property,
//       value: value
//     },
//     {
//       name: unindexedProperty,
//       value: value,
//       excludeFromIndexes: true
//     }
//   ]
function toDatastore(obj, nonIndexed) {
    nonIndexed = nonIndexed || [];
    let results = [];
    Object.keys(obj).forEach((k) => {
        if (obj[k] === undefined) {
            return;
        }
        results.push({
            name: k,
            value: obj[k],
            excludeFromIndexes: nonIndexed.indexOf(k) !== -1
        });
    });
    return results;
}

function getByEmailAndPassword(email, password, cb) {
    const encodedePassword = saltHashPassword(password);
    const query = ds.createQuery([kind])
        .filter('email', '=', email)
        .filter('password', encodedePassword)
        .limit(1);

    ds.runQuery(query, (error, entity) => {
        if (error) {
            cb(error, null);
            return;
        }
        if (stringutil.isEmpty(entity)) {
            return (null, null);
            return;
        }

        cb(null, fromDatastore(entity[0]));
        return;
    });
}

function getByEmail(email, cb) {
    const query = ds.createQuery([kind])
        .filter('email', '=', email)
        .limit(1);
    ds.runQuery(query, (error, entity) => {
        if (error) {
            cb(error, null);
            return;
        }

        if (stringutil.isEmpty(entity)) {
            cb(null, null);
            return;
        }

        cb(null, fromDatastore(entity[0]));
        return;
    });
}

function getByEmailAndActiveKey(email, active_key, cb) {
    const query = ds.createQuery([kind])
        .filter('email', '=', email)
        .filter('active_key', '=', active_key)
        .limit(1);

    ds.runQuery(query, (err, entity) => {
        if (err) {
            cb(err);
            return;
        }

        if (stringutil.isEmpty(entity)) {
            cb(null, null)
            return;
        }
        cb(null, fromDatastore(entity[0]));
    });
}

function update(id, dataBody, cb) {
    let data = dataFormatFromBody(dataBody);
    if (!isValidData(data)) {
        cb('invalid datas');
        return;
    }

    let key;
    if (id) { // update
        key = ds.key([kind, parseInt(id, 10)]);
    } else {
        key = ds.key(kind);
        data.enable = false;
        data.active_key = randomString(7, data.email);
        logging.info(key);
    }
    data.password = saltHashPassword(data.password);

    const entity = {
        key: key,
        data: toDatastore(data, ['description'])
    };

    logging.info(data);

    ds.save(
        entity,
        (err) => {
            if (err) {
                cb(err);
                return;
            }
            data.id = entity.key.id;
            cb(null, data);
            return;
        }
    );
}

module.exports = {
    create: (data, cb) => {
        update(null, data, cb);
    },
    getByEmail: getByEmail,
    getByEmailAndPassword: getByEmailAndPassword
};
