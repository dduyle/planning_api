'use-strict';

const express = require('express');
const bodyParser = require('body-parser');
const config = require('../../config');
const model = require('./model');
const logging = require('../../lib/logging');
const responseuitl = require('../util/responseutil');

function getModel() {
    return require('./model');
}

let router = express.Router();
router.use(bodyParser.json());

router.get('/', (req, res, next) => {
  res.json('yooooo');
});

module.exports = router;
