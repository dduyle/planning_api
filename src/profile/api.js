'use-strict';

const express = require('express');
const bodyParser = require('body-parser');
const config = require('../../config');
const model = require('./model');
const logging = require('../../lib/logging');
const responseutil = require('../util/responseutil');

function getModel() {
    return require('./model');
}

const router = express.Router();

router.use(bodyParser.json());

/**
 * POST /profile/create
 *
 * Create a new user
 */
router.post('/create', (req, res, next) => {
    const body = req.body;

    if (typeof body.email == undefined) {
        next(responseutil.bad_request());
        return;
    }

    model.getByEmail(body.email, (err, entity) => {
        if (err || entity == null) {
            model.create(req.body, (error, createdUser) => {
                if (error) {
                    next(responseutil.internal_error(error));
                    return;
                }

                let userResponse = {
                    id: createdUser.id,
                    active_key: createdUser.active_key
                };

                res.json(responseutil.created(userResponse));
            });
        } else {
            logging.info(entity);
            if (!entity.enable) {
                next(responseutil.conflict('User not activated'));
                return;
            }

            next(responseutil.conflict('User existed'));
            return;
        }

    })

});

router.get('/', (req, res, next) => {
    res.json("ok get profile");
});

module.exports = router;
